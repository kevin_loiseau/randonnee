# Symfony5 Docker Development Stack
This is a lightweight stack based on Alpine Linux for running Symfony5 into Docker containers using docker-compose. 

[![Build Status](https://travis-ci.org/coloso/symfony-docker.svg?branch=master)](https://travis-ci.org/coloso/symfony-docker)
### Prerequisites
* [Docker](https://www.docker.com/)

### Container
 - [nginx](https://pkgs.alpinelinux.org/packages?name=nginx&branch=v3.10) 1.18.+
 - [php-fpm](https://pkgs.alpinelinux.org/packages?name=php7&branch=v3.10) 7.4.+
    - [composer](https://getcomposer.org/) 
- [mysql](https://hub.docker.com/_/mysql/) 5.7.+

### Installing

1st time to build:
```
 docker-compose up --build
```
Only run:
```
 docker-compose up
```
Enter in php volume to exectute php commands:
```
 docker-compose exec php sh
```
install dependencies:
```
 composer install
```
Get tables in database
```
 run : php bin/console doctrine:migrations:migrate
```

call [localhost](http://localhost:81/api) in browser to show api description.
 
