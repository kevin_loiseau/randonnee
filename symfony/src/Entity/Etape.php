<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EtapeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=EtapeRepository::class)
 */
class Etape
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $parcours;

    /**
     * @ORM\ManyToOne(targetEntity=Itineraire::class, inversedBy="etapes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $itineraire;

    /**
     * @ORM\ManyToOne(targetEntity=Lieu::class, inversedBy="etapes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lieu;

    public function __construct(string $nom, string $parcours, Itineraire $itineraire, Lieu $lieu){
        $this->nom = $nom;
        $this->parcours = $parcours;
        $this->itineraire = $itineraire;
        $this->lieu = $lieu;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getParcours(): ?string
    {
        return $this->parcours;
    }

    public function setParcours(?string $parcours): self
    {
        $this->parcours = $parcours;

        return $this;
    }

    public function getItineraire(): ?Itineraire
    {
        return $this->itineraire;
    }

    public function setItineraire(?Itineraire $itineraire): self
    {
        $this->itineraire = $itineraire;

        return $this;
    }

    public function getLieu(): ?Lieu
    {
        return $this->lieu;
    }

    public function setLieu(?Lieu $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }
}
