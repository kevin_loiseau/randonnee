<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Repository\CommentaireRepository;
use App\Repository\LieuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Builder\NormalizationBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @Route("/api/commentaire")
 */
class CommentaireController extends AbstractController
{


    /**
     * @Route("", name="commentaire_new", methods={"POST"})
     * @param Request $request
     * @param LieuRepository $lieuRepository
     * @param NormalizerInterface $normalizer
     * @return Response
     */
    public function new(Request $request,
                        LieuRepository $lieuRepository,
                        NormalizerInterface $normalizer): Response
    {
        $response = new Response();
        $data = json_decode($request->getContent(), true);
        try {
            $pseudo = $data['pseudo'];
            $commentaire= $data['commentaire'];
            $commentaireObj = new Commentaire();
            $commentaireObj->setPseudo($pseudo);
            $commentaireObj->setCommentaire($commentaire);
            $commentaireObj->setLieu($lieuRepository->findOneBy(['id'=>$data['idLieu']]));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($commentaireObj);
            $entityManager->flush();

            $response = $this->responseFactory($response, $normalizer->normalize([
                'id' => $commentaireObj->getId(),
                'pseudo' => $commentaireObj->getPseudo(),
                'commendtaire' => $commentaireObj->getCommentaire()
            ]), 200);

        }catch (\Exception $exception){
            $message = "impossible d'ajouter le commentaire";
            $response=$this->responseFactory($response, $message, 410);
        } catch (ExceptionInterface $e) {
            $message = "impossible d'ajouter le commentaire";
            $response=$this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }
    }


    /**
     * @Route("/{id}", name="commentaire_delete", methods={"DELETE"})
     * @param CommentaireRepository $commentaireRepository
     * @param NormalizerInterface $normalizer
     * @param int $id
     * @return Response
     */
    public function delete(CommentaireRepository $commentaireRepository, NormalizerInterface $normalizer, int $id): Response
    {
        $responde = new Response();
        $commentaire = $commentaireRepository->findOneBy(['id'=> $id]);
        if (!empty($commentaire)){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($commentaire);
            $entityManager->flush();

            $message = "Supprimé";
        }else{
            $message = "Il n'existe pas de commentaire donc l'id vaudrait : ".$id.".";
        }

        return $this->responseFactory($responde, $message, 200);
    }

    /**
     * @param Response $response
     * @param $toSerialize
     * @param int $statusCode
     * @return Response
     */
    private function responseFactory(Response $response, $toSerialize, int $statusCode):Response{
        $jsonMessage = json_encode($toSerialize);
        $response->setContent($jsonMessage);
        $response->setStatusCode($statusCode);
        $response->setCharset('utf-8');
        $response->headers->set("Content-Type", "application/json");
        return $response;
    }
}
