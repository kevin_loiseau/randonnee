<?php

namespace App\Controller;

use App\Entity\Itineraire;
use App\Repository\EtapeRepository;
use App\Repository\ItineraireRepository;
use App\Repository\LieuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @Route("/api/itineraire")
 */
class ItineraireController extends AbstractController
{
    /**
     * @Route("", name="itineraires_index", methods={"GET"})
     * @param ItineraireRepository $itineraireRepository
     * @param LieuRepository $lieuRepository
     * @param EtapeRepository $etapeRepository
     * @param NormalizerInterface $normalizer
     * @return Response
     * @throws ExceptionInterface
     */
    public function index(ItineraireRepository $itineraireRepository,
                          LieuRepository $lieuRepository,
                          EtapeRepository $etapeRepository,
                          NormalizerInterface $normalizer): Response
    {
        $response = new Response();
        $itineraires = $itineraireRepository->findAll();
        try {
            if (!empty($itineraires)) {
                $itinerairesNormalized = [];
                foreach ($itineraires as $itineraire){
                    $etapes = $etapeRepository->findBy(["itineraire" => $itineraire]);
                    $etapesNormalized = $this->etapesFactory($etapes, $normalizer, $lieuRepository);
                    $itinerairesNormalized[] = $this->itineraireFactory($itineraire, $normalizer, $etapesNormalized);
                }
                $response = $this->responseFactory($response, $itinerairesNormalized, 200);
            }else{
                $message = "Il n'existe pas d'Itinéraire.";
                $response = $this->responseFactory($response, $message, 200);
            }
        }catch (\Exception $e){
            $message = "La récupération des Itinéraires à échoué.";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }

    }

    /**
     * @Route("", name="itineraire_new", methods={"POST"})
     * @param Request $request
     * @param NormalizerInterface $normalizer
     * @return Response
     */
    public function new(Request $request, NormalizerInterface $normalizer): Response
    {
        $data = json_decode($request->getContent(), true);
        $response = new Response();

        try {
            $nom = $data["nom"];
            $niveau = $data["niveau"];
            $itineraire = new Itineraire($nom, $niveau);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($itineraire);
            $entityManager->flush();

            $itineraireNormalizer = $normalizer->normalize($itineraire);
            $response = $this->responseFactory($response, $itineraireNormalizer, 200);
        }catch (ExceptionInterface $e){
            $message = "L'objet Itineraire doit être de la forme : {'nom':'value', 'niveau':'value'}";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @Route("/{id}", name="itineraire_show", methods={"GET"})
     * @param ItineraireRepository $itineraireRepository
     * @param EtapeRepository $etapeRepository
     * @param LieuRepository $lieuRepository
     * @param NormalizerInterface $normalizer
     * @param string $id
     * @return Response
     */
    public function show(ItineraireRepository $itineraireRepository,
                         EtapeRepository $etapeRepository,
                         LieuRepository $lieuRepository,
                         NormalizerInterface $normalizer,
                         string $id): Response
    {
        $response = new Response();
        try {
            $itineraire = $itineraireRepository->findOneBy(['id' => $id]);
            if (!empty($itineraire)){
                $etapes = $etapeRepository->findBy(['itineraire' => $itineraire]);
                $etapesNormalized = $this->etapesFactory($etapes, $normalizer, $lieuRepository);
                $itineraireNormalized = $this->itineraireFactory($itineraire, $normalizer, $etapesNormalized);
                $response = $this->responseFactory($response, $itineraireNormalized, 200);
            }else{
                $message = "Il n'existe pas d'itinéraire dont l'id vaudrait ".$id.'.';
                $response = $this->responseFactory($response, $message, 200);
            }
        } catch (ExceptionInterface $e) {
            $message = "Impossible de récupérer l'étape dont l'id vaud ".$id.".";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @Route("/{id}", name="itineraire_edit", methods={"PUT"})
     * @param Request $request
     * @param NormalizerInterface $normalizer
     * @param ItineraireRepository $itineraireRepository
     * @param string $id
     * @return Response
     */
    public function edit(Request $request,
                         NormalizerInterface $normalizer,
                         ItineraireRepository $itineraireRepository, string $id): Response
    {
        $data = json_decode($request->getContent(), true);
        $response = new Response();
        try {
            $nom = $data["nom"];
            $niveau = $data["niveau"];
            $itineraire = $itineraireRepository->findOneBy(['id' => $id]);
            $itineraire->setNom($nom);
            $itineraire->setNiveau($niveau);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $itineraireNormalizer = $normalizer->normalize($itineraire);
            $response = $this->responseFactory($response, $itineraireNormalizer, 200);
        }catch (ExceptionInterface $e){
            $message = "L'objet Itineraire doit être de la forme : {'nom':'value', 'niveau':'value'}";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @Route("/{id}", name="itineraire_delete", methods={"DELETE"})
     * @param ItineraireRepository $itineraireRepository
     * @param int $id
     * @return Response
     */
    public function delete(ItineraireRepository $itineraireRepository, int $id): Response
    {
        $response = new Response();
        $itineraire = $itineraireRepository->findOneBy(['id' => $id]);
        if (!empty($itineraire)) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($itineraire);
            $entityManager->flush();

            $message = "L'itineraire dont l'id vaut ".$id." a été supprimé.";
            $response = $this->responseFactory($response, $message, 200);
        }else{
            $message = "L'itineraire dont l'id vaut ".$id.", n'existe pas. Impossible d'appliquer la suppression.";
            $response = $this->responseFactory($response, $message, 200);
        }
        return $response;
    }

    /*******FACTORIES********/

    /**
     * @param $etapes
     * @param NormalizerInterface $normalizer
     * @param LieuRepository $lieuRepository
     * @return array
     * @throws ExceptionInterface
     */
    private function etapesFactory($etapes,
                                   NormalizerInterface $normalizer,
                                   LieuRepository $lieuRepository) :array {
        $etapesNormalized = [];
        foreach ($etapes as $etape) {
            $lieu = $lieuRepository->findOneBy(['id' => $etape->getLieu()->getId()]);
            $etapeProp = [
                "id" => $etape->getId(),
                "nom" => $etape->getNom(),
                "parcours" => $etape->getParcours(),
                "lieu" => [
                    "id" => $lieu->getId(),
                    "nom" => $lieu->getNom(),
                    "description" => $lieu->getDescription()
                ]
            ];
            $etapesNormalized[] = $normalizer->normalize($etapeProp);
        }
        return $etapesNormalized;
    }

    /**
     * @param Itineraire $itineraire
     * @param NormalizerInterface $normalizer
     * @param array $etapesNormalized
     * @return array
     * @throws ExceptionInterface
     */
    private function itineraireFactory(Itineraire $itineraire,
                                       NormalizerInterface $normalizer,
                                       array $etapesNormalized): array {
        $itineraireProp = [
            "id" => $itineraire->getId(),
            "nom" => $itineraire->getNom(),
            "niveau" => $itineraire->getNiveau(),
            "etapes" => $etapesNormalized
        ];
        return $normalizer->normalize($itineraireProp);
    }

    /**
     * @param Response $response
     * @param $toSerialize
     * @param int $statusCode
     * @return Response
     */
    private function responseFactory(Response $response, $toSerialize, int $statusCode):Response{
        $jsonMessage = json_encode($toSerialize);
        $response->setContent($jsonMessage);
        $response->setStatusCode($statusCode);
        $response->setCharset('utf-8');
        $response->headers->set("Content-Type", "application/json");
        return $response;
    }



}
