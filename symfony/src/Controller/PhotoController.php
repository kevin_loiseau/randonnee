<?php

namespace App\Controller;

use App\Entity\Photo;
use App\Form\PhotoType;
use App\Repository\LieuRepository;
use App\Repository\PhotoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @Route("/api/photo")
 */
class PhotoController extends AbstractController
{
    /**
     * @Route("", name="photo_index", methods={"GET"})
     * @param PhotoRepository $photoRepository
     * @param NormalizerInterface $normalizer
     * @return Response
     * @throws ExceptionInterface
     */
    public function index(PhotoRepository $photoRepository, NormalizerInterface $normalizer): Response
    {
        $response = new Response();
        $photos = $photoRepository->findAll();
        try {
            if (!empty($photos)){
                $photoNormilized=$this->photoFactory($photos, $normalizer);
                $response = $this->responseFactory($response, $photoNormilized, 200);
            }else{
                $message = "Il n'existe pas de photo.";
                $response = $this->responseFactory($response, $message, 200);
            }
        }catch (\Exception $exception){
            $message = "Quelque chose s'est mal passé, impossible de récupérer les photos";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @Route("", name="photo_new", methods={"POST"})
     * @param Request $request
     * @param LieuRepository $lieuRepository
     * @param NormalizerInterface $normalizer
     * @return Response
     */
    public function new(Request $request,
                        LieuRepository $lieuRepository,
                        NormalizerInterface $normalizer): Response
    {
        $data = json_decode($request->getContent(), true);
        $response = new Response();

        try{
            $pseudo = $data['pseudo'];
            $path = $data['path'];
            $idLieu = $data['idLieu'];
            $photo = new Photo();
            $photo->setPseudo($pseudo);
            $photo->setPath($path);
            $photo->setLieu($lieuRepository->findOneBy(['id'=> $idLieu]));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($photo);
            $entityManager->flush();

            $response = $this->responseFactory($response, $normalizer->normalize([
                'id' => $photo->getId(),
                'pseudo' => $photo->getPseudo(),
                'path' => $photo->getPath()
            ]), 200);
        }catch (\Exception $exception){
            $message = "Quelqu chose s'est mal passé, impossible d'ajouter la photo";
            $response = $this->responseFactory($response, $message, 410);
        } catch (ExceptionInterface $e) {
        } finally {
            return $response;
        }
    }

    /*******FACTORIES*******/

    /**
     * @param array $photos
     * @param NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    private function photoFactory(array $photos, NormalizerInterface $normalizer): array {
        $photosNormalized = [];
        foreach ($photos as $photo){
            $photosNormalized[] = $normalizer->normalize([
                "id" => $photo->getId(),
                "pseudo" => $photo->getPseudo(),
                "path" => $photo->getPath()
            ]);
        }
        return $photosNormalized;
    }

    /**
     * @param Response $response
     * @param $toSerialize
     * @param int $statusCode
     * @return Response
     */
    private function responseFactory(Response $response, $toSerialize, int $statusCode):Response{
        $jsonMessage = json_encode($toSerialize);
        $response->setContent($jsonMessage);
        $response->setStatusCode($statusCode);
        $response->setCharset('utf-8');
        $response->headers->set("Content-Type", "application/json");
        return $response;
    }
}
