<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Repository\CommentaireRepository;
use App\Repository\EtapeRepository;
use App\Repository\ItineraireRepository;
use App\Repository\LieuRepository;
use App\Repository\PhotoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Etape;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class EtapeController
 * @package App\Controller
 * @Route("/api/etape")
 */
class EtapeController extends AbstractController
{

    /**
     * @Route("", name="etape_index", methods={"GET"})
     * @param EtapeRepository $etapeRepository ;
     * @param CommentaireRepository $commentaireRepository
     * @param PhotoRepository $photoRepository
     * @param NormalizerInterface $normalizer
     * @param LieuRepository $lieuRepository
     * @return Response
     * @throws ExceptionInterface
     */
    public function index(EtapeRepository $etapeRepository,
                          CommentaireRepository $commentaireRepository,
                          PhotoRepository $photoRepository,
                          NormalizerInterface $normalizer,
                          LieuRepository $lieuRepository): Response
    {
        $response = new Response();
        try {
            $etapes = $etapeRepository->findAll();
            if (!empty($etapes)){
                $etapesNormalized = [];
                foreach ($etapes as $etape){
                    $lieu = $lieuRepository->findOneBy(['id'=>$etape->getLieu()->getId()]);
                    $lieuNormalized = $this->lieufactory($lieu, $normalizer, $commentaireRepository, $photoRepository);
                    $etapesNormalized[] = $this->etapesFactory($etape, $normalizer, $lieuNormalized);
                }
                $response = $this->responseFactory($response, $etapesNormalized, 200);
            }else{
                $message = "Il n'existe pas d'étapes";
                $response = $this->responseFactory($response, $message, 200);
            }
        }catch (\Exception $e){
            $message = "Il y a eu un problème";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }

    }

    /**
     * @Route("", name="etape_new", methods={"POST"})
     * @param ItineraireRepository $itineraireRepository
     * @param LieuRepository $lieuRepository
     * @param Request $request
     * @param CommentaireRepository $commentaireRepository
     * @param PhotoRepository $photoRepository
     * @param NormalizerInterface $normalizer
     * @return Response
     * @throws ExceptionInterface
     */
    public function new(ItineraireRepository $itineraireRepository,
                             LieuRepository $lieuRepository,
                             Request $request,
                             CommentaireRepository $commentaireRepository,
                             PhotoRepository $photoRepository,
                             NormalizerInterface $normalizer) : Response
    {
        $data = json_decode($request->getContent(), true);
        $response = new Response();

        try{
            $nom = $data["nom"];
            $parcours = $data["parcours"];
            $lieu = $lieuRepository->findOneBy(["id" =>intVal($data["idLieu"])]);
            $itineraire = $itineraireRepository->find(intVal($data["idItineraire"]));
            $etape = new Etape($nom, $parcours, $itineraire, $lieu);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($etape);
            $entityManager->flush();

            $lieu = $lieuRepository->findOneBy(['id'=>$etape->getLieu()->getId()]);
            $lieuNormalized = $this->lieufactory($lieu, $normalizer, $commentaireRepository, $photoRepository);
            $response = $this->responseFactory($response, $this->etapesFactory($etape, $normalizer, $lieuNormalized), 200);
        }catch (\Exception $exception){
            $message = "L'objet Etape doit être de la forme : {'nom':'value', 'parcours':'value', 'idLieu':'value', 'iditineraire':'value'}";
           $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }
    }


    /**
     * @Route("/{id}", name="etape_show", methods={"GET"})
     * @param LieuRepository $lieuRepository
     * @param EtapeRepository $etapeRepository
     * @param NormalizerInterface $normalizer
     * @param CommentaireRepository $commentaireRepository
     * @param PhotoRepository $photoRepository
     * @param string $id
     * @return Response
     */
    public function show(LieuRepository $lieuRepository,
                         EtapeRepository $etapeRepository,
                         NormalizerInterface $normalizer,
                         CommentaireRepository $commentaireRepository,
                         PhotoRepository $photoRepository,
                         string $id) : Response
    {
        $response = new Response();
        try {
            $etape = $etapeRepository->findOneBy(['id'=> $id]);
            if (!empty($etape)) {
                $lieu = $lieuRepository->findOneBy(['id'=>$etape->getLieu()->getId()]);
                $lieuNormalized = $this->lieufactory($lieu, $normalizer, $commentaireRepository, $photoRepository);
                $response = $this->responseFactory($response, $this->etapesFactory($etape, $normalizer, $lieuNormalized), 200);
            } else {
                $message = "Il n'existe pas d'étape dont l'id vaudrait ".$id.'.';
                $response = $this->responseFactory($response, $message, 200);
            }
        } catch (ExceptionInterface $e) {
            $message = "Impossible de récupérer l'étape dont l'id vaut ".$id.".";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @Route ("/{id}", name="etape_edit", methods={"PUT"})
     * @param Request $request
     * @param LieuRepository $lieuRepository
     * @param ItineraireRepository $itineraireRepository
     * @param NormalizerInterface $normalizer
     * @param EtapeRepository $etapeRepository
     * @param CommentaireRepository $commentaireRepository
     * @param PhotoRepository $photoRepository
     * @param string $id
     * @return Response
     */
    public function edit(Request $request,
                         LieuRepository $lieuRepository,
                         ItineraireRepository $itineraireRepository,
                         NormalizerInterface $normalizer,
                         EtapeRepository $etapeRepository,
                         CommentaireRepository $commentaireRepository,
                         PhotoRepository $photoRepository,
                         string $id): Response
    {
        $data = json_decode($request->getContent(), true);
        $response = new Response();
        try {
            $etape = $etapeRepository->findOneBy(['id' => $id]);
            if(!empty($etape)){
                $nom = $data["nom"];
                $parcours = $data["parcours"];
                $idLieu = intVal($data["idLieu"]);
                $idItineraire= intVal($data["idItineraire"]);

                $lieu = $lieuRepository->findOneBy(["id" =>$idLieu]);
                $itineraire = $itineraireRepository->findOneBy(['id'=>$idItineraire]);
                $etape->setNom($nom);
                $etape->setParcours($parcours);
                $etape->setLieu($lieu);
                $etape->setItineraire($itineraire);

                $em = $this->getDoctrine()->getManager();
                $em->flush();

                $lieuNormalized = $this->lieufactory($lieu, $normalizer, $commentaireRepository, $photoRepository);
                $response = $this->responseFactory($response, $this->etapesFactory($etape, $normalizer, $lieuNormalized), 200);
            }else{
                $message = "L'étape dont l'id vaut : ".$id." n'existe pas";
                $response = $this->responseFactory($response, $message, 410);
            }
        } catch (ExceptionInterface $e) {
            $message = "L'objet Etape doit être de la forme : {'nom' : 'value', 'parcours' : 'value', 'idLieu': 'id du lieu', 'idItineraire': 'id de l'itineraire'}";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @Route("/{id}"), name = "etape_delete", methods={"DELETE"}
     * @param EtapeRepository $etapeRepository
     * @param int $id
     * @return Response
     */
    public function delete(EtapeRepository $etapeRepository, int $id): Response
    {
        $response = new Response();
        $etape = $etapeRepository->findOneBy(['id' => $id]);
        if (!empty($etape)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($etape);
            $em->flush();

            $message = "L'étape dont l'id vaut ".$id." a été supprimé.";
            $response = $this->responseFactory($response, $message, 200);
        }else{
            $message = "L'étape dont l'id vaut ".$id.", n'existe pas. Impossible d'appliquer la suppression.";
            $response = $this->responseFactory($response, $message, 200);
        }
        return $response;
    }

    /********Factories*********/
    /**
     * @param Etape $etape
     * @param NormalizerInterface $normalizer
     * @param array $lieuxNormalized
     * @return array
     * @throws ExceptionInterface
     */
    private function etapesFactory(Etape $etape,
                                   NormalizerInterface $normalizer,
                                   array $lieuxNormalized) :array {
        return $normalizer->normalize([
            "id" => $etape->getId(),
            "nom" => $etape->getNom(),
            "parcours" => $etape->getParcours(),
            "lieu" => $lieuxNormalized
        ]);
    }

    /**
     * @param Lieu $lieu
     * @param NormalizerInterface $normalizer
     * @param CommentaireRepository $commentaireRepository
     * @param PhotoRepository $photoRepository
     * @return array
     * @throws ExceptionInterface
     */
    private function lieufactory(Lieu $lieu,
                                 NormalizerInterface $normalizer,
                                 CommentaireRepository $commentaireRepository,
                                 PhotoRepository $photoRepository) :array{
        $commentaires = $commentaireRepository->findBy(['lieu'=>$lieu]);
        $photos = $photoRepository->findBy(['lieu'=>$lieu]);
        return $normalizer->normalize([
            "id" => $lieu->getId(),
            "nom" => $lieu->getNom(),
            "description" => $lieu->getDescription(),
            "commentaires" => $this->commentairesFactory($commentaires, $normalizer),
            "photos" => $this->photoFactory($photos, $normalizer)
        ]);
    }

    /**
     * @param array $commentaires
     * @param NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    private function commentairesFactory(array $commentaires, NormalizerInterface $normalizer):array{
        $commentairesNormalized = [];
        foreach ($commentaires as $commentaire){
            $commentairesNormalized[] = $normalizer->normalize([
                "id" => $commentaire->getId(),
                "pseudo" => $commentaire->getPseudo(),
                "commentaire" => $commentaire->getCommentaire()
            ]);
        }
        return $commentairesNormalized;
    }

    /**
     * @param array $photos
     * @param NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    private function photoFactory(array $photos, NormalizerInterface $normalizer): array {
        $photosNormalized = [];
        foreach ($photos as $photo){
            $photosNormalized[] = $normalizer->normalize([
                "id" => $photo->getId(),
                "pseudo" => $photo->getPseudo(),
                "path" => $photo->getPath()
            ]);
        }
        return $photosNormalized;
    }

    /**
     * @param Response $response
     * @param $toSerialize
     * @param int $statusCode
     * @return Response
     */
    private function responseFactory(Response $response, $toSerialize, int $statusCode):Response{
        $response->setContent(json_encode($toSerialize));
        $response->setStatusCode($statusCode);
        $response->setCharset('utf-8');
        $response->headers->set("Content-Type", "application/json");
        return $response;
    }
}