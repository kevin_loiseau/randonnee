<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Repository\CommentaireRepository;
use App\Repository\LieuRepository;
use App\Repository\PhotoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @Route("/api/lieu")
 */
class LieuController extends AbstractController
{
    /**
     * @Route("", name="lieu_index", methods={"GET"})
     * @param LieuRepository $lieuRepository
     * @param CommentaireRepository $commentaireRepository
     * @param PhotoRepository $photoRepository
     * @param NormalizerInterface $normalizer
     * @return Response
     */
    public function index(LieuRepository $lieuRepository,
                          CommentaireRepository $commentaireRepository,
                          PhotoRepository $photoRepository,
                          NormalizerInterface $normalizer): Response
    {
        $response = new Response();
        try {
            $lieux = $lieuRepository->findAll();
            if (!empty($lieux)){
                $lieuxNormalized = [];
                foreach ($lieux as $lieu){
                    $commentaires = $commentaireRepository->findBy(["lieu" => $lieu]);
                    $photos = $photoRepository->findBy(["lieu" => $lieu]);
                    $commentairesNormalized = $this->commentairesFactory($commentaires, $normalizer);
                    $photosNormalized = $this->photoFactory($photos, $normalizer);
                    $lieuxNormalized[] = $this->lieufactory($lieu, $normalizer, $commentairesNormalized, $photosNormalized);
                }
                $response = $this->responseFactory($response, $lieuxNormalized, 200);
            }else{
                $message = "Il n'existe aucun lieu.";
                $response = $this->responseFactory($response, $message, 200);
            }
        }catch (ExceptionInterface $e){
            $message = "Impossible de récupérer les lieux.";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @Route("", name="lieu_new", methods={"POST"})
     * @param Request $request
     * @param NormalizerInterface $normalizer
     * @return Response
     */
    public function new(Request $request, NormalizerInterface $normalizer): Response
    {
        $data = json_decode($request->getContent(), true);
        $response = new Response();
        try {
            $nom = $data["nom"];
            $description = $data["description"];
            $lieu = new Lieu($nom, $description);

            $em = $this->getDoctrine()->getManager();
            $em->persist($lieu);
            $em->flush();

            $lieuNormalized = $normalizer->normalize($lieu);
            $response = $this->responseFactory($response, $lieuNormalized, 200);
        }catch (\Exception $exception){
            $message = "Problème !";
            $response = $this->responseFactory($response, $message, 410);
        }catch (ExceptionInterface $e){
            $message = "L'objet Lieu doit être sous la form : {'nom':'string', 'description':'string'}";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @Route("/{id}", name="lieu_edit", methods={"PUT"})
     * @param Request $request
     * @param NormalizerInterface $normalizer
     * @param LieuRepository $lieuRepository
     * @param int $id
     * @return Response
     */
    public function edit(Request $request,
                         NormalizerInterface $normalizer,
                         LieuRepository $lieuRepository,
                         int $id): Response {
        $data = json_decode($request->getContent(), true);
        $response = new Response();
        try {
            $nom = $data["nom"];
            $description = $data["description"];
            $lieu = $lieuRepository->findOneBy(['id'=> $id]);
            $lieu->setNom($nom);
            $lieu->setDescription($description);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $lieuNormalized = $normalizer->normalize($lieu);
            $response = $this->responseFactory($response, $lieuNormalized, 200);
        }catch (\Exception $exception){
            $message = "Il y a eu un problème";
            $response = $this->responseFactory($response, $message, 410);
        } catch (ExceptionInterface $e) {
            $message = "Il y a eu un problème lors de la normalisation de l'objet";
            $response = $this->responseFactory($response, $message, 410);
        } finally {
            return $response;
        }

    }

    /**
     * @Route("/{id}", name="lieu_delete", methods={"DELETE"})
     * @param LieuRepository $lieuRepository
     * @param int $id
     * @return Response
     */
    public function delete(LieuRepository $lieuRepository, int $id): Response {
        $response = new Response();
        $lieu = $lieuRepository->findOneBy(['id'=> $id]);
        if (!empty($lieu)){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lieu);
            $entityManager->flush();

            $message = "Le lieu dont l'id vaut ".$id." a été supprimé.";
            $response = $this->responseFactory($response, $message, 200);
        }else{
            $message = "Le lieu dont l'id vaut ".$id.", n'existe pas. Impossible d'appliquer la suppression.";
            $response = $this->responseFactory($response, $message, 200);
        }
        return $response;
    }

    /*******FACTORIES*******/


    /**
     * @param Lieu $lieu
     * @param NormalizerInterface $normalizer
     * @param array $commentaires
     * @param array $photos
     * @return array
     * @throws ExceptionInterface
     */
    private function lieufactory(Lieu $lieu, NormalizerInterface $normalizer, ?array $commentaires, ?array $photos) :array{
        return $normalizer->normalize([
            "id" => $lieu->getId(),
            "nom" => $lieu->getNom(),
            "description" => $lieu->getDescription(),
            "commentaires" => $commentaires,
            "photos" => $photos
        ]);
    }

    /**
     * @param array $commentaires
     * @param NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    private function commentairesFactory(array $commentaires, NormalizerInterface $normalizer):array{
        $commentairesNormalized = [];
        foreach ($commentaires as $commentaire){
            $commentairesNormalized[] = $normalizer->normalize([
                "id" => $commentaire->getId(),
                "pseudo" => $commentaire->getPseudo(),
                "commentaire" => $commentaire->getCommentaire()
            ]);
        }
        return $commentairesNormalized;
    }

    /**
     * @param array $photos
     * @param NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    private function photoFactory(array $photos, NormalizerInterface $normalizer): array {
        $photosNormalized = [];
        foreach ($photos as $photo){
            $photosNormalized[] = $normalizer->normalize([
                "id" => $photo->getId(),
                "pseudo" => $photo->getPseudo(),
                "path" => $photo->getPath()
            ]);
        }
        return $photosNormalized;
    }

    /**
     * @param Response $response
     * @param $toSerialize
     * @param int $statusCode
     * @return Response
     */
    private function responseFactory(Response $response, $toSerialize, int $statusCode):Response{
        $response->setContent(json_encode($toSerialize));
        $response->setStatusCode($statusCode);
        $response->setCharset('utf-8');
        $response->headers->set("Content-Type", "application/json");
        return $response;
    }
}
